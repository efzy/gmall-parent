package com.atguigu.gmall.common.util;

import com.atguigu.gmall.common.constant.RedisConst;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lfy
 * @Description
 * @create 2022-12-16 14:56
 */
public class UserAuthUtil {

    /**
     * 获取老请求
     *
     * @return
     */
    public static HttpServletRequest request() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attributes.getRequest();
    }


    /**
     * 获取登录的用户id
     * @return
     */
    public static Long getUserId() {
        HttpServletRequest request = request();
        String header = request.getHeader(RedisConst.USER_ID_HEADER);
        if (StringUtils.isEmpty(header)) {
            return null;
        }
        return Long.parseLong(header);
    }
}
