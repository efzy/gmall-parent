package com.atguigu.gmall.feign.ware;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lfy
 * @Description
 * @create 2022-12-21 15:32
 */
//feign可以发送任何http请求
//url：精确指定当前feignclient以后发送请求的基准地址
@FeignClient(value = "ware-manage",url = "http://localhost:9001")
public interface WareFeignClient {

    // http://localhost:9001/hasStock?skuId=43&num=2
    @GetMapping("/hasStock")
    String hasStock(@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num);

}
