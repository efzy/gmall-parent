package com.atguigu.gmall.feign.cart;

import com.atguigu.gmall.cart.entity.CartInfo;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.entity.SkuInfo;
import feign.Request;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-16 10:48
 */
@RequestMapping("/api/inner/rpc/cart")
@FeignClient("service-cart")
public interface CartFeignClient {

    /**
     * 把商品添加到购物车
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/add/{skuId}/{num}")
    Result<SkuInfo> addToCart(@PathVariable("skuId") Long skuId,
                              @PathVariable("num") Integer num,
                              Request.Options options);


    /**
     * 删除选中的
     * @return
     */
    @DeleteMapping("/deleteChecked")
    Result deleteChecked();


    /**
     * 获取所有选中的商品
     * @return
     */
    @GetMapping("/checkeds")
    Result<List<CartInfo>> getChecked();
}
