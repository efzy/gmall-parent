package com.atguigu.gmall.feign.user;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.user.entity.UserAddress;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-21 14:46
 */
@RequestMapping("/api/inner/rpc/user")
@FeignClient("service-user")
public interface UserFeignClient {

    /**
     * 返回用户收货地址列表
     * @param userId
     * @return
     */
    @GetMapping("/addresses/{userId}")
    Result<List<UserAddress>> getUserAddress(@PathVariable("userId")
                                                            Long userId);
}
