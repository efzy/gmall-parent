package com.atguigu.gmall.feign.search;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.search.Goods;
import com.atguigu.gmall.search.vo.SearchParamVo;
import com.atguigu.gmall.search.vo.SearchRespVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 14:36
 */
@RequestMapping("/api/inner/rpc/search")
@FeignClient("service-search")
public interface SearchFeignClient {

    @GetMapping("/hotscore/{skuId}/{score}")
    Result updateHotScore(@PathVariable("skuId") Long skuId,
                                 @PathVariable("score") Long score);
    /**
     * 检索商品
     * @param searchParamVo
     * @return
     */
    @PostMapping("/searchgoods")
    Result<SearchRespVo> search(@RequestBody SearchParamVo searchParamVo);

    /**
     * 商品上架
     * @return
     */
    @PostMapping("/up/goods")
    Result up(@RequestBody Goods goods);

    /**
     * 下架
     * @param skuId
     * @return
     */
    @GetMapping("/down/goods/{skuId}")
    Result down(@PathVariable("skuId") Long skuId);
}
