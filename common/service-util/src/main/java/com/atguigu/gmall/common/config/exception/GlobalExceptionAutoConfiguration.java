package com.atguigu.gmall.common.config.exception;

import com.atguigu.gmall.common.config.exception.handler.GlobalExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author lfy
 * @Description
 * @create 2022-12-10 11:01
 */
@Import(GlobalExceptionHandler.class)
@Configuration
public class GlobalExceptionAutoConfiguration {


}
