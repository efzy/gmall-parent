package com.atguigu.gmall.common.config.exception.annotation;

import com.atguigu.gmall.common.config.exception.GlobalExceptionAutoConfiguration;
import org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author lfy
 * @Description
 * @create 2022-12-10 11:03
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import(GlobalExceptionAutoConfiguration.class)
public @interface EnableAutoExceptionHandler {
}
