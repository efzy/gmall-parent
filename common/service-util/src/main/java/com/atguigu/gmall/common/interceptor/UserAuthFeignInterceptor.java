package com.atguigu.gmall.common.interceptor;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.common.util.UserAuthUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lfy
 * @Description
 * @create 2022-12-16 14:05
 */
@Component
public class UserAuthFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
//        HttpServletRequest request = CartController.map
//                .get(Thread.currentThread());
//        HttpServletRequest request = CartController.threadLocal.get();

        //1、想要获取老请求
        HttpServletRequest request = UserAuthUtil.request();

        String userId = request.getHeader(RedisConst.USER_ID_HEADER);
        if(!StringUtils.isEmpty(userId)){
            //userId传下去
            template.header(RedisConst.USER_ID_HEADER,userId);
        }

        String tempId = request.getHeader(RedisConst.TEMP_ID_HEADER);
        if(!StringUtils.isEmpty(tempId)){
            //tempId传下去
            template.header(RedisConst.TEMP_ID_HEADER,tempId);
        }

    }
}
