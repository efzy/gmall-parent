package com.atguigu.gmall.common.config.mq;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.util.MD5;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author lfy
 * @Description
 * @create 2022-12-23 15:32
 */
@Slf4j
@Service
public class MqService {


    private RabbitTemplate rabbitTemplate;
    private StringRedisTemplate redisTemplate;

    public MqService(RabbitTemplate rabbitTemplate,StringRedisTemplate redisTemplate){
        this.rabbitTemplate = rabbitTemplate;
        this.redisTemplate = redisTemplate;
        initTemplate();
    }


    /**
     * 发消息
     * @param message
     * @param exchange
     * @param routingKey
     */
    public void send(Object message,String exchange,String routingKey){
        rabbitTemplate.convertAndSend(exchange,routingKey, JSON.toJSONString(message));
    }

    /**
     * 有限次数重试
     * @param channel
     * @param tag
     * @param content
     * @param retryCount
     * @throws IOException
     */
    public void retry(Channel channel, long tag, String content, Integer retryCount) throws IOException {
        String md5 = MD5.encrypt(content);
        //2、同一个消息最多重试5次
        Long increment = redisTemplate.opsForValue().increment("msg:count:" + md5);
        if(increment <= retryCount){
            log.info("消费失败；重新入队；次数：{}",increment);  //导致无限入队消费，cpu打满
            channel.basicNack(tag,false,true);
        }else {
            log.error("消费重试超过最大限定次数：已达到{}，记录到数据库，不在重试，等待人工处理",increment);
            redisTemplate.delete("msg:count:" + md5);
            channel.basicAck(tag,false);
        }
    };



    private void initTemplate() {
        this.rabbitTemplate.setConfirmCallback((CorrelationData correlationData,
                                           boolean ack,
                                           String cause)->{
            log.info("confirm回调： data:{},ack:{},cause:{}",correlationData,ack,cause);
            //发不成功的消息要记录到后台数据库，等待人工处理
        });

        this.rabbitTemplate.setReturnCallback((Message message,
                                          int replyCode,
                                          String replyText,
                                          String exchange,
                                          String routingKey)->{
            log.info("return回调： message:{},replyCode:{},replyText:{},exchange:{},routingKey:{}",
                    message,replyCode,replyText,exchange,routingKey);
            //TODO 发不成功的消息要记录到后台数据库，等待人工处理
        });

        rabbitTemplate.setRetryTemplate(new RetryTemplate());
    }
}
