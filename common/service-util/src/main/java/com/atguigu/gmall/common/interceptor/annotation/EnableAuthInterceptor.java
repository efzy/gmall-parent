package com.atguigu.gmall.common.interceptor.annotation;

import com.atguigu.gmall.common.config.thread.AppThreadPoolAutoConfiguration;
import com.atguigu.gmall.common.interceptor.UserAuthFeignInterceptor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author lfy
 * @Description  开始认证拦截器功能
 * @create 2022-12-21 14:35
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({UserAuthFeignInterceptor.class})
public @interface EnableAuthInterceptor {
}
