package com.atguigu.gmall.product.init;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.product.entity.SkuInfo;
import com.atguigu.gmall.product.service.SkuInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lfy
 * @Description 项目启动就创建
 * @create 2022-12-06 14:38
 */

@Slf4j
public class InitListener implements SpringApplicationRunListener{

    public InitListener(SpringApplication application, String[] args){
        log.info("监听器对象创建： application：{}； args:{}",application,args);
        this.application = application;
    }



    SpringApplication application;




    @Override
    public void started(ConfigurableApplicationContext context) {
        log.info("监听到项目started；skuInfoService:{}");
    }

    @Override
    public void starting() {
        log.info("监听到项目starting");
    }


}
