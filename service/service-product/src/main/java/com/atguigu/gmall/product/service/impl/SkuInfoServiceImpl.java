package com.atguigu.gmall.product.service.impl;
import com.atguigu.gmall.product.entity.*;
import com.atguigu.gmall.product.service.*;
import com.atguigu.gmall.product.vo.CategoryTreeVo;
import com.atguigu.gmall.search.SearchAttr;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Date;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.feign.search.SearchFeignClient;
import com.atguigu.gmall.product.vo.SkuSaveVo;
import com.atguigu.gmall.search.Goods;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.product.mapper.SkuInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Administrator
 * @description 针对表【sku_info(库存单元表)】的数据库操作Service实现
 * @createDate 2022-11-29 11:42:45
 */
@Slf4j
@Service
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoMapper, SkuInfo>
        implements SkuInfoService {

    @Autowired
    SkuImageService skuImageService;

    @Autowired
    SkuAttrValueService attrValueService;

    @Autowired
    SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    BaseTrademarkService baseTrademarkService;


    @Transactional
    @Override
    public void saveSkuInfoData(SkuSaveVo vo) {
        log.info("sku保存： {}", vo);
        //1、 把基本信息保存到 sku_info
        SkuInfo skuInfo = new SkuInfo();
        BeanUtils.copyProperties(vo, skuInfo);
        save(skuInfo);

        //提取到刚保存的sku的自增id
        Long skuId = skuInfo.getId();

        //2、图片信息： sku_image
        List<SkuImage> skuImages = vo.getSkuImageList()
                .stream()
                .map(item -> {
                    SkuImage image = new SkuImage();
                    BeanUtils.copyProperties(item,image);
                    image.setSkuId(skuId);
                    return image;
                }).collect(Collectors.toList());
        skuImageService.saveBatch(skuImages);

        //3、平台属性：  sku_attr_value
        List<SkuAttrValue> attrValues = vo.getSkuAttrValueList()
                .stream()
                .map(item -> {
                    SkuAttrValue value = new SkuAttrValue();
                    BeanUtils.copyProperties(item,value);
                    value.setSkuId(skuId);
                    return value;
                }).collect(Collectors.toList());
        attrValueService.saveBatch(attrValues);

        //4、销售属性： sku_sale_attr_value
        List<SkuSaleAttrValue> saleAttrValues = vo.getSkuSaleAttrValueList()
                .stream()
                .map(item -> {
                    SkuSaleAttrValue attrValue = new SkuSaleAttrValue();

                    attrValue.setSkuId(skuId);
                    attrValue.setSpuId(skuInfo.getSpuId());
                    attrValue.setSaleAttrValueId(item.getSaleAttrValueId());

                    return attrValue;
                }).collect(Collectors.toList());
        skuSaleAttrValueService.saveBatch(saleAttrValues);


        //同步bitmap
        redisTemplate.opsForValue().setBit(RedisConst.SKUID_BITMAP,skuId,true);
    }



    @Autowired
    SearchFeignClient searchFeignClient;
    @Autowired
    BaseCategoryService baseCategoryService;

    @Autowired
    SkuAttrValueService skuAttrValueService;

    @Override
    public void upGoods(Long skuId) {
        //1、修改数据库的上架状态
        boolean update = lambdaUpdate()
                .set(SkuInfo::getIsSale, 1)
                .eq(SkuInfo::getId, skuId).update();

        //2、保存到es中
        if(update){
            Goods goods = prepareGoods(skuId);
            searchFeignClient.up(goods);
            log.info("商品【{}】 上架完成",skuId);
        }

    }


    //根据skuid准备一个要上架的商品数据
    private Goods prepareGoods(Long skuId) {
        SkuInfo info = getById(skuId);
        Goods goods = new Goods();
        goods.setId(info.getId());
        goods.setDefaultImg(info.getSkuDefaultImg());
        goods.setTitle(info.getSkuName());
        goods.setPrice(info.getPrice().doubleValue());
        goods.setCreateTime(new Date());

        //查询品牌
        BaseTrademark trademark = baseTrademarkService.getById(info.getTmId());
        goods.setTmId(info.getTmId());
        goods.setTmName(trademark.getTmName());
        goods.setTmLogoUrl(trademark.getLogoUrl());


        //三级分类信息
        CategoryTreeVo tree = baseCategoryService.getCategoryTreeWithC3Id(info.getCategory3Id());
        goods.setCategory1Id(tree.getCategoryId());
        goods.setCategory1Name(tree.getCategoryName());

        CategoryTreeVo child = tree.getCategoryChild().get(0);
        goods.setCategory2Id(child.getCategoryId());
        goods.setCategory2Name(child.getCategoryName());

        CategoryTreeVo child2 = child.getCategoryChild().get(0);
        goods.setCategory3Id(child2.getCategoryId());
        goods.setCategory3Name(child2.getCategoryName());

        //热度分
        goods.setHotScore(0L);

        //所有的平台属性
        List<SearchAttr> attrs = skuAttrValueService.getSkuAttrsAndValue(skuId);
        goods.setAttrs(attrs);
        return goods;
    }

    @Override
    public void downGoods(Long skuId) {
        //1、修改数据库的上架状态
        boolean update = lambdaUpdate()
                .set(SkuInfo::getIsSale, 0)
                .eq(SkuInfo::getId, skuId).update();

        //2、远程下架
        if(update){
            searchFeignClient.down(skuId);
            //上下架的细节：缓存、bitmap都要同步
            log.info("商品【{}】 下架完成",skuId);
        }
    }
}




