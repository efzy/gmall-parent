package com.atguigu.gmall.product.init;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.product.entity.SkuInfo;
import com.atguigu.gmall.product.service.SkuInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-06 15:27
 */
@Slf4j
@Component
public class InitRunner implements CommandLineRunner {
    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    SkuInfoService skuInfoService;

    public void initBitMap() {
        log.info("正在初始化 skuid - bitmap");
        List<SkuInfo> ids = skuInfoService.lambdaQuery()
                .select(SkuInfo::getId)
                .list();
        ids.stream()
                .forEach(item -> {
                    redisTemplate.opsForValue().setBit(RedisConst.SKUID_BITMAP,item.getId(),true);
                });
        log.info("初始化 skuid - bitmap 完成");
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("InitRunner... 启动run；组件：skuInfoService:{}",skuInfoService);
        initBitMap();
    }
}
