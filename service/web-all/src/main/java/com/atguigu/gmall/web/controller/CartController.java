package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.feign.cart.CartFeignClient;
import com.atguigu.gmall.product.entity.SkuInfo;
import feign.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author lfy
 * @Description
 * @create 2022-12-16 10:40
 */
@Controller
public class CartController {


    @Autowired
    CartFeignClient cartFeignClient;

//    public static Map<Thread,HttpServletRequest> map = new ConcurrentHashMap<>();


    /**
     * 同一个线程共享数据用 ThreadLocal： 防止OOM：
     * 1、强： var = 对象； 只要 var!=null，就不会回收
     * 2、弱： WeakReference： 崩的前一刻回收
     *     JVM发现马上要OOM了，就会优先回收掉WeakReference包装的东西
     * 3、软： SoftReference：只要下次发送gc就直接回收掉；
     * 4、虚： 纯粹就是无引用
     */
//    public static ThreadLocal<HttpServletRequest> threadLocal = new ThreadLocal<>();

    /**
     * 把商品添加到购物车
     *
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/addCart.html")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("skuNum") Integer num,
                            Model model) {
        //远程调用购物车  OOM；
        //List、Map、Set容器类。一定要想清楚，容器里面的数据什么时候放，一定要有删。

//        map.put(Thread.currentThread(),request);
//        threadLocal.set(request);
        //controller的所有逻辑调用是否是同一个线程
        Request.Options options = new Request.Options(5000,5000);
        Result<SkuInfo> result = cartFeignClient.addToCart(skuId, num,options);

        //远程结束了，拦截器运行完了
//        threadLocal.remove();
//        map.remove(Thread.currentThread()); //防止OOM
        //{skuDefaultImg、skuName、id}
        model.addAttribute("skuInfo", result.getData());

        model.addAttribute("skuNum", num);
        return "cart/addCart";//添加成功提示页
    }

    /**
     * 购物车列表页
     * 页面渲染：
     * 1、服务端渲染： 服务器负责把页面的全部内容组装完全返回
     * 2、客户端渲染：
     *   - 客户端给服务器发送ajax请求
     *   - 服务器把数据返给浏览器，客户端把数据填充到这个页面
     * @return
     */
    @GetMapping("/cart.html")
    public String cartListPage(){

        return "cart/index";
    }


    /**
     * 删除选中的商品
     * @return
     */
    @GetMapping("/cart/deleteChecked")
    public String deleteChecked(){


        cartFeignClient.deleteChecked();

        return "redirect:http://cart.gmall.com/cart.html";
    }
}
