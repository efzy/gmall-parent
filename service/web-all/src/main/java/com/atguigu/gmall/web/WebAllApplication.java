package com.atguigu.gmall.web;

import com.atguigu.gmall.common.interceptor.annotation.EnableAuthInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.lang.reflect.Proxy;

/**
 * @author lfy
 * @Description
 * @create 2022-12-03 9:03
 */

//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
//@EnableDiscoveryClient
//@EnableCircuitBreaker
//SpringBoot：只会扫描自己主程序所在的包和子包组件
//com.atguigu.gmall.feign
@EnableAuthInterceptor
@EnableFeignClients(basePackages = {
        "com.atguigu.gmall.feign"
//        "com.atguigu.gmall.feign.item",
//        "com.atguigu.gmall.feign.search"
})
@SpringCloudApplication
public class WebAllApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebAllApplication.class, args);

    }
}
