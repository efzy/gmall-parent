package com.atguigu.gmall.web.filter;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lfy
 * @Description
 * @create 2022-12-15 14:12
 */
public class HelloFilter extends HttpFilter {


    /**
     *
     * @param request  请求
     * @param response 响应
     * @param chain    过滤器链
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilter(HttpServletRequest request,
                            HttpServletResponse response,
                            FilterChain chain) throws IOException, ServletException {
        //1、当前线程作为key
        //1、放行之前改请求
        //放行请求
        chain.doFilter(request,response);
        //2、放行之后改响应
//        response.getWriter().write();
        //重定向原理
        //响应状态码：  302
        //响应头： Location: 新的位置
        response.sendRedirect("aaaaa");
    }
}
