package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.feign.search.SearchFeignClient;
import com.atguigu.gmall.search.vo.SearchParamVo;
import com.atguigu.gmall.search.vo.SearchRespVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 11:39
 */
@Controller
public class SearchController {

    @Autowired
    SearchFeignClient searchFeignClient;

    @GetMapping("/list.html")
    public String search(SearchParamVo param,
                         Model model){

        //远程调用检索服务
        SearchRespVo searchData = searchFeignClient.search(param).getData();

        //1、检索参数;SearchParamVo
        model.addAttribute("searchParam",searchData.getSearchParam());

        //2、品牌面包屑;  字符串
        model.addAttribute("trademarkParam",searchData.getTrademarkParam());

        //3、平台属性面包屑; 集合；[{attrName、attrValue、attrId}]
        model.addAttribute("propsParamList",searchData.getPropsParamList());


        //4、品牌列表： 集合 [{tmId、tmName、tmLogoUrl}]
        model.addAttribute("trademarkList",searchData.getTrademarkList());

        //5、属性列表： 集合 [{attrName、attrValueList(字符串集合)、attrId}]
        model.addAttribute("attrsList",searchData.getAttrsList());

        //6、url参数
        model.addAttribute("urlParam",searchData.getUrlParam());

        //7、排序信息（type、sort）
        model.addAttribute("orderMap",searchData.getOrderMap());

        //8、商品列表 集合[{每个商品信息}]
        model.addAttribute("goodsList",searchData.getGoodsList());

        //9、页码
        model.addAttribute("pageNo",searchData.getPageNo());

        //10、总页码
        model.addAttribute("totalPages",searchData.getTotalPages());


        return "list/index"; //检索结果展示页
    }
}
