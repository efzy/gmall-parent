package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.result.ResultCodeEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lfy
 * @Description
 * @create 2022-12-10 10:38
 */

@RestController
public class HelloTestController {

    @GetMapping("/div")
    public Result div(@RequestParam("num") Long num, HttpServletResponse response){

//        Cookie cookie = new Cookie("jsessionidxxx","123");
//        cookie.setDomain(".jd.com"); //直接发令牌的时候放大
//        response.addCookie(cookie);
        long i = 10/num;
        return Result.ok(i);
    }


//    /**
//     * 处理这个controller的所以异常
//     * @return
//     */
//    @ExceptionHandler({Exception.class})
//    public Result handleException(Exception e){
//        Result<Object> fail = Result.fail();
//        fail.setMessage(e.getMessage());
//        return fail;
//    }
}
