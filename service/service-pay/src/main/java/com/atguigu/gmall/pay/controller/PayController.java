package com.atguigu.gmall.pay.controller;

import com.alipay.api.AlipayApiException;
import com.atguigu.gmall.common.util.UserAuthUtil;
import com.atguigu.gmall.pay.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lfy
 * @Description
 * @create 2022-12-24 9:57
 */
@RequestMapping("/api/payment")
@RestController
public class PayController {

    @Autowired
    PayService payService;

    /**
     * 请求二维码收银台页面
     * @param orderId
     * @return
     * @throws AlipayApiException
     */
    @GetMapping("/alipay/submit/{orderId}")
    public String alipay(@PathVariable("orderId") Long orderId) throws AlipayApiException {

        Long userId = UserAuthUtil.getUserId();

        String page = payService.generatePayPage(orderId,userId);

        return page;
    }
}
