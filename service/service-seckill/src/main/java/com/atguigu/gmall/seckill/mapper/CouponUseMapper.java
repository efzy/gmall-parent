package com.atguigu.gmall.seckill.mapper;

import com.atguigu.gmall.seckill.entity.CouponUse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【coupon_use(优惠券领用表)】的数据库操作Mapper
* @createDate 2022-12-27 08:49:20
* @Entity com.atguigu.gmall.seckill.entity.CouponUse
*/
public interface CouponUseMapper extends BaseMapper<CouponUse> {

}




