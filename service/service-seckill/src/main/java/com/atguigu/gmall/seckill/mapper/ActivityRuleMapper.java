package com.atguigu.gmall.seckill.mapper;

import com.atguigu.gmall.seckill.entity.ActivityRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【activity_rule(优惠规则)】的数据库操作Mapper
* @createDate 2022-12-27 08:49:20
* @Entity com.atguigu.gmall.seckill.entity.ActivityRule
*/
public interface ActivityRuleMapper extends BaseMapper<ActivityRule> {

}




