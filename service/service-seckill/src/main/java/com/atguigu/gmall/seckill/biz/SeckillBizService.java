package com.atguigu.gmall.seckill.biz;

import com.atguigu.gmall.common.result.ResultCodeEnum;
import com.atguigu.gmall.seckill.vo.SeckillOrderConfirmVo;
import com.atguigu.gmall.seckill.vo.SeckillOrderSubmitVo;

/**
 * @author lfy
 * @Description
 * @create 2022-12-27 10:21
 */
public interface SeckillBizService {
    /**
     * 生成秒杀码
     * @param skuId
     * @return
     */
    String generateSeckillCode(Long skuId);


    /**
     * 秒杀下单：秒杀就开始真正排队，能抢到库存的人就下单成功，走后续流程，抢不到的人下单失败
     * @param skuId
     * @param code
     */
    void seckillOrder(Long skuId, String code);

    /**
     * 检查秒杀单状态
     * @param skuId
     * @return
     */
    ResultCodeEnum checkOrder(Long skuId);

    /**
     * 获取秒杀单数据
     * @param code
     * @return
     */
    SeckillOrderConfirmVo getSeckillOrderInfo(String code);

    /**
     * 提交秒杀单
     * @param submitVo
     * @return
     */
    Long submitOrder(SeckillOrderSubmitVo submitVo);
}
