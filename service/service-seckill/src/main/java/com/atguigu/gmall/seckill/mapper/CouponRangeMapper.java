package com.atguigu.gmall.seckill.mapper;

import com.atguigu.gmall.seckill.entity.CouponRange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【coupon_range(优惠券范围表)】的数据库操作Mapper
* @createDate 2022-12-27 08:49:20
* @Entity com.atguigu.gmall.seckill.entity.CouponRange
*/
public interface CouponRangeMapper extends BaseMapper<CouponRange> {

}




