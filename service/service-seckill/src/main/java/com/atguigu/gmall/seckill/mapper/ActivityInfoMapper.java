package com.atguigu.gmall.seckill.mapper;

import com.atguigu.gmall.seckill.entity.ActivityInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【activity_info(活动表)】的数据库操作Mapper
* @createDate 2022-12-27 08:49:20
* @Entity com.atguigu.gmall.seckill.entity.ActivityInfo
*/
public interface ActivityInfoMapper extends BaseMapper<ActivityInfo> {

}




