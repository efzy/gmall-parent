package com.atguigu.gmall.seckill.service;

import com.atguigu.gmall.seckill.entity.CouponInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lfy
* @description 针对表【coupon_info(优惠券表)】的数据库操作Service
* @createDate 2022-12-27 08:49:20
*/
public interface CouponInfoService extends IService<CouponInfo> {

}
