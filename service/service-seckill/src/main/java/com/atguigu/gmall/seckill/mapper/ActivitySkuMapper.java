package com.atguigu.gmall.seckill.mapper;

import com.atguigu.gmall.seckill.entity.ActivitySku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【activity_sku(活动参与商品)】的数据库操作Mapper
* @createDate 2022-12-27 08:49:20
* @Entity com.atguigu.gmall.seckill.entity.ActivitySku
*/
public interface ActivitySkuMapper extends BaseMapper<ActivitySku> {

}




