package com.atguigu.gmall.user.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.user.entity.UserInfo;
import com.atguigu.gmall.user.service.UserInfoService;
import com.atguigu.gmall.user.vo.LoginSuccessVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author lfy
 * @Description
 * @create 2022-12-15 10:37
 */
@RequestMapping("/api/user")
@RestController
public class LoginController {



    @Autowired
    UserInfoService userInfoService;
    /**
     * 登录
     * @param info
     * @return
     */
    @PostMapping("/passport/login")
    public Result login(@RequestBody UserInfo info){

        LoginSuccessVo vo = userInfoService.login(info);
        //远程调用购物车进行合并？ 现场等结果要做剩下的事情
        //像一些某些事件触发以后，其他人要做一些事情的，适合用消息队列完成事件通知机制
        //懒思想
        return Result.ok(vo);
    }


    /**
     * 退出
     * 删除用户在redis中的登录信息
     * @return
     */
    @GetMapping("/passport/logout")
    public Result logout(@RequestHeader("token") String token){

        userInfoService.logout(token);
        return Result.ok();
    }
}
