package com.atguigu.gmall.user.mapper;

import com.atguigu.gmall.user.entity.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lfy
* @description 针对表【user_address(用户地址表)】的数据库操作Mapper
* @createDate 2022-12-15 09:40:04
* @Entity com.atguigu.gmall.user.entity.UserAddress
*/
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}




