package com.atguigu.gmall.user.service;

import com.atguigu.gmall.user.entity.UserInfo;
import com.atguigu.gmall.user.vo.LoginSuccessVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lfy
* @description 针对表【user_info(用户表)】的数据库操作Service
* @createDate 2022-12-15 09:40:04
*/
public interface UserInfoService extends IService<UserInfo> {

    /**
     * 用户登录
     * @param info
     * @return
     */
    LoginSuccessVo login(UserInfo info);

    /**
     * 用户退出
     * @param token
     */
    void logout(String token);
}
