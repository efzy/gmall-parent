package com.atguigu.gmall.user.service;

import com.atguigu.gmall.user.entity.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lfy
* @description 针对表【user_address(用户地址表)】的数据库操作Service
* @createDate 2022-12-15 09:40:04
*/
public interface UserAddressService extends IService<UserAddress> {

}
