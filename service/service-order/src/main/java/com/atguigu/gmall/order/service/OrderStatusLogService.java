package com.atguigu.gmall.order.service;

import com.atguigu.gmall.order.entity.OrderStatusLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lfy
* @description 针对表【order_status_log】的数据库操作Service
* @createDate 2022-12-21 10:10:41
*/
public interface OrderStatusLogService extends IService<OrderStatusLog> {

}
