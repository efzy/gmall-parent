package com.atguigu.gmall.order.rpc;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.UserAuthUtil;
import com.atguigu.gmall.order.biz.OrderBizService;
import com.atguigu.gmall.order.entity.OrderInfo;
import com.atguigu.gmall.order.service.OrderInfoService;
import com.atguigu.gmall.order.vo.OrderConfirmRespVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author lfy
 * @Description
 * @create 2022-12-21 14:12
 */
@RequestMapping("/api/inner/rpc/order")
@RestController
public class OrderRpcController {


    @Autowired
    OrderBizService orderBizService;

    @Autowired
    OrderInfoService orderInfoService;

    /**
     * 根据订单id查询订单
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public Result<OrderInfo> getOrderInfoById(@PathVariable("id") Long id){

        Long userId = UserAuthUtil.getUserId();
        OrderInfo orderInfo = orderInfoService.getOrderInfoByIdAndUserId(id,userId);

        return Result.ok(orderInfo);
    }

    /**
     * 获取订单确认页数据
     * @return
     */
    @GetMapping("/confirmdata")
    public Result<OrderConfirmRespVo> orderConfirmData(){

        OrderConfirmRespVo respVo = orderBizService.getConfirmData();

        return Result.ok(respVo);
    }


    /**
     * 保存秒杀单
     * @param info
     * @return
     */
    @PostMapping("/seckill/order")
    public Result<Long> saveSeckillOrder(@RequestBody OrderInfo info){

        Long orderId =  orderBizService.saveSeckillOrder(info);
        return Result.ok(orderId);
    }
}
