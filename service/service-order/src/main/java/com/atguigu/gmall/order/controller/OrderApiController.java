package com.atguigu.gmall.order.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.mq.ware.WareStockMsg;
import com.atguigu.gmall.order.biz.OrderBizService;
import com.atguigu.gmall.order.vo.OrderSplitReps;
import com.atguigu.gmall.order.vo.OrderSubmitVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-23 8:55
 */
@RequestMapping("/api/order")
@RestController
public class OrderApiController {


    @Autowired
    OrderBizService orderBizService;


    @PostMapping("/orderSplit")
    public List<OrderSplitReps> orderSplit(@RequestParam("orderId") Long orderId,
                                           @RequestParam("wareSkuMap") String json){

        //拆单：把一个大订单，拆分成多个子订单
        List<OrderSplitReps> splitReps =  orderBizService.orderSplit(orderId,json);

        return splitReps;
    }



    @PostMapping("/auth/submitOrder")
    public Result submitOrder(@RequestParam("tradeNo") String tradeNo,
                              @Valid @RequestBody OrderSubmitVo submitVo){

        //下单方法； 雪花算法生成的大数字id，一定要以字符串的方式返回出去
        Long orderId = orderBizService.submitOrder(submitVo,tradeNo);
        return Result.ok(orderId+"");
    }
}
