package com.atguigu.gmall.cart.rpc;

import com.atguigu.gmall.cart.entity.CartInfo;
import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.entity.SkuInfo;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lfy
 * @Description 购物车远程调用接口
 * @create 2022-12-16 10:45
 */
@RequestMapping("/api/inner/rpc/cart")
@RestController
public class CartRpcController {

    @Autowired
    CartService cartService;

    /**
     * 把商品添加到购物车
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/add/{skuId}/{num}")
    public Result addToCart(@PathVariable("skuId") Long skuId,
                            @PathVariable("num") Integer num){

        //1、用哪个购物车
        String cartKey = cartService.determinCartKey();

        //2、把指定商品添加到指定购物车
        SkuInfo skuInfo = cartService.addToCart(skuId,num,cartKey);

        return Result.ok(skuInfo);
    }

    @DeleteMapping("/deleteChecked")
    public Result deleteChecked(){

        String cartKey = cartService.determinCartKey();

        cartService.deleteChecked(cartKey);

        return Result.ok();
    }

    /**
     * 获取所有选中的商品
     * @return
     */
    @GetMapping("/checkeds")
    public Result<List<CartInfo>> getChecked(){

        String cartKey = cartService.determinCartKey();

        List<CartInfo> checkeds = cartService.getCheckeds(cartKey);

        return Result.ok(checkeds);
    }
}
