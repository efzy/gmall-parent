package com.atguigu.gmall.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lfy
 * @Description
 * @create 2022-12-16 9:58
 */
@EnableFeignClients(basePackages = "com.atguigu.gmall.feign.product")
@SpringCloudApplication
public class CartApplication {

    public static void main(String[] args) {
        SpringApplication.run(CartApplication.class,args);

    }
}
