package com.atguigu.gmall.item;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.product.entity.SkuImage;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lfy
 * @Description
 * @create 2022-12-09 14:01
 */
public class GenericTest {

    @Test
    void test02(){
        List<SkuImage> images = new ArrayList<>();
        SkuImage image = new SkuImage();
        image.setId(0L);
        image.setSkuId(0L);
        image.setImgName("0");
        image.setImgUrl("0");
        image.setSpuImgId(0L);
        image.setIsDefault("0");
        images.add(image);
        SkuImage image2 = new SkuImage();
        image2.setId(1L);
        image2.setSkuId(1L);
        image2.setImgName("1");
        image2.setImgUrl("1");
        image2.setSpuImgId(0L);
        image2.setIsDefault("1");
        images.add(image2);

        String json = JSON.toJSONString(images);
        System.out.println(json);
        Type type = null;
        for (Method method : GenericTest.class.getDeclaredMethods()) {
            if (method.getName().equals("getHaha")) {
                type = method.getGenericReturnType();
            }
        }

        Object o = JSON.parseObject(json, type);
        System.out.println("逆转的数据："+o);
    }

    @Test
    void test01(){
        for (Method method : GenericTest.class.getDeclaredMethods()) {
            System.out.println(method.getName()+"===>"+method.getGenericReturnType()+"==>"+method.getDeclaringClass());
        }
    }

    List<SkuImage> getHaha(){
        return null;
    }

    String getHehe(){
        return "";
    }

    Map<String,List<SkuImage>> getHeihei(){
        return null;
    }
}
