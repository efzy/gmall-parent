package com.atguigu.gmall.item;

import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.UUID;

/**
 * @author lfy
 * @Description
 * @create 2022-12-09 14:36
 */
public class ExpressionTest {


    @Test
    void test02(){
        //1、创建一个表达式解析器
        ExpressionParser parser = new SpelExpressionParser();
        //2、解析一个表达式；定界符  //sku:info:#{#args[1]}
        Expression expression = parser.parseExpression("#{1+1}",
                ParserContext.TEMPLATE_EXPRESSION);

        //3、得到值
        EvaluationContext ec = new StandardEvaluationContext();
        ec.setVariable("args",new Long[]{88L,77L,45L});
//        ec.setVariable("sms",new SmsService());
        String value = expression.getValue(ec, String.class);
        System.out.println(value);
    }

    @Test
    void test01(){
        //1、创建一个表达式解析器
        ExpressionParser parser = new SpelExpressionParser();
        //2、解析一个表达式
        String s = UUID.randomUUID().toString();
        Expression expression = parser.parseExpression("T(java.util.UUID).randomUUID().toString()");
        //3、得到表达式计算出来的值
        Object value = expression.getValue();
        System.out.println("最终的值："+value);
    }

    public static class Person {
        public String haha(){
            System.out.println("hahaha....");
            return "666";
        }
    }
}
