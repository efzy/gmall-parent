package com.atguigu.gmall.item;

import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author lfy
 * @Description
 * @create 2022-12-08 13:55
 */
public class RedissonTest {


    @Test
    void  redissonclient(){
        //1、创建配置
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://192.168.200.100:6379")
                .setPassword("Lfy123!@!");
        //2、创建客户端
        RedissonClient client = Redisson.create(config);
        System.out.println(client);


    }
}
