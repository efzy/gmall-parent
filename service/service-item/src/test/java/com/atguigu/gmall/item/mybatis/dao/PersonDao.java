package com.atguigu.gmall.item.mybatis.dao;

import com.atguigu.gmall.item.mybatis.annotation.MySQL;

/**
 * @author lfy
 * @Description
 * @create 2022-12-09 9:33
 */
public interface PersonDao {


    @MySQL("select count(*) from person")
    Integer getAllPersonCount();

    @MySQL("insert into person('age','email') values({age},{email})")
    void insertPerson(Integer age,String email);
}
