package com.atguigu.gmall.item;

import com.atguigu.gmall.starter.cache.annotation.EnableAppCache;
import com.atguigu.gmall.common.config.thread.annotation.EnableAppThreadPool;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author lfy
 * @Description
 * @create 2022-12-03 11:20
 *
 * Spring：Redis
 * 1、导入starter
 * 2、写配置就行
 */
@EnableAppThreadPool
//用谁扫谁、允许bean定义信息重写
@EnableFeignClients(basePackages = {
        "com.atguigu.gmall.feign.product",
        "com.atguigu.gmall.feign.search"
})
@SpringCloudApplication
public class ItemApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItemApplication.class,args);
    }
}
