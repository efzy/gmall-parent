package com.atguigu.gmall.item.service;

/**
 * @author lfy
 * @Description
 * @create 2022-12-08 9:24
 */
public interface LockService {
    String lock() throws InterruptedException;

    void unlock(String uuid);

}
