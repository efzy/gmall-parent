package com.atguigu.gmall.search;

import com.atguigu.gmall.search.bean.Person;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.Query;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 9:57
 */
@SpringBootTest
public class EsTest {

    @Autowired
    ElasticsearchRestTemplate restTemplate;


    @Test
    void testSearch(){
        //1、创建一个原生检索。代表能使用原生的DSL进行检索
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must(QueryBuilders.rangeQuery("age").gte(20L));
        boolQuery.must(QueryBuilders.matchQuery("name","张"));

        //========原生检索： 利用QueryBuilders 创建一个检索条件=======
        Query query = new NativeSearchQuery(boolQuery);

        //2、检索命中的记录信息
        SearchHits<Person> hits = restTemplate.search(query, Person.class, IndexCoordinates.of("person"));


        System.out.println("检索结果："+hits);

        for (SearchHit<Person> hit : hits.getSearchHits()) {
            Person person = hit.getContent();
            System.out.println(person);
        }
    }

    @Test
    void testDeleteIndex(){
        boolean person = restTemplate.indexOps(IndexCoordinates.of("person"))
                .delete();
        System.out.println(person);
    }

    @Test
    void testDelete(){
        String person = restTemplate.delete("1", IndexCoordinates.of("person"));
        System.out.println(person);
    }

    @Test
    void testQuery(){
        Person person = restTemplate.get("1", Person.class, IndexCoordinates.of("person"));
        System.out.println(person);
    }


    @Test
    void testPost() {
        IndexQuery query = new IndexQueryBuilder()
                .withId("3")
                .withObject(new Person(3L, "李五", 21))
                .build();
        restTemplate.index(query, IndexCoordinates.of("person"));
    }


    @Test
    void testCreatIndex() {
//        new DefaultIndexOperations();
        boolean person = restTemplate.indexOps(IndexCoordinates.of("person"))
                .create();
        System.out.println(person);
    }


    @Test
    void test01() {
        System.out.println(restTemplate);

//        restTemplate.createIndex("hello");


        restTemplate.deleteIndex("hello");
        System.out.println("删除hello索引完成...");

    }
}
