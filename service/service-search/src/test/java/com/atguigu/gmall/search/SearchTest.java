package com.atguigu.gmall.search;

import com.atguigu.gmall.search.service.SearchService;
import com.atguigu.gmall.search.vo.SearchParamVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author lfy
 * @Description
 * @create 2022-12-13 10:12
 */
@SpringBootTest
public class SearchTest {

    @Autowired
    SearchService searchService;

    @Test
    void testSearch(){

        //category3Id=61&trademark=2:华为&props=4:256GB:机身存储&props=3:8GB:运行内存&
        // order=2:desc&pageNo=1&keyword=华为
        SearchParamVo paramVo = new SearchParamVo();
        paramVo.setCategory3Id(61L);
        paramVo.setTrademark("2:华为");
        paramVo.setProps(new String[]{"4:256GB:机身存储","3:8GB:运行内存"});
        paramVo.setOrder("2:desc");
        paramVo.setPageNo(1);
        paramVo.setKeyword("华为");

        searchService.search(paramVo);
    }
}
