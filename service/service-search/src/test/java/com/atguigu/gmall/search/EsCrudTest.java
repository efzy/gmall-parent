package com.atguigu.gmall.search;

import com.atguigu.gmall.search.bean.Person;
import com.atguigu.gmall.search.repo.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 11:10
 */
@SpringBootTest
public class EsCrudTest {
    @Autowired
    PersonRepository personRepository;

    @Test
    void testQuery(){
        List<Person> all = personRepository.findAllByAgeGreaterThanEqual(19);
        all.forEach(item->{
            System.out.println(item);
        });
    }


    @Test
    void testCrud(){
        List<Person> people = Arrays.asList(
                new Person(1L, "张三", 18),
                new Person(2L, "张四", 19),
                new Person(3L, "李三", 20),
                new Person(4L, "李四", 21)
        );
        personRepository.saveAll(people);
        System.out.println("保存完成....");

        Iterable<Person> all = personRepository.findAll();
        all.forEach(item->{
            System.out.println(item);
        });
    }
}
