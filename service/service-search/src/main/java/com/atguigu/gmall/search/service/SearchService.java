package com.atguigu.gmall.search.service;

import com.atguigu.gmall.search.Goods;
import com.atguigu.gmall.search.vo.SearchParamVo;
import com.atguigu.gmall.search.vo.SearchRespVo;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 14:34
 */
public interface SearchService {
    /**
     * 检索
     * @param searchParamVo
     * @return
     */
    SearchRespVo search(SearchParamVo searchParamVo);

    /**
     * 上架：保存到es
     * @param goods
     */
    void up(Goods goods);

    /**
     * 下架
     * @param skuId
     */
    void down(Long skuId);

    /**
     * 修改热度分
     * @param skuId
     * @param score
     */
    void updateHotScore(Long skuId, Long score);
}
