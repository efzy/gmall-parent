package com.atguigu.gmall.search.repo;

import com.atguigu.gmall.search.Goods;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author lfy
 * @Description  先装好ik分词器再写这个
 * @create 2022-12-12 15:36
 */
@Repository
public interface GoodsRepository extends PagingAndSortingRepository<Goods,Long> {


}
