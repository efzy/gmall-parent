package com.atguigu.gmall.search.repo;

import com.atguigu.gmall.search.bean.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 11:07
 */
@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person,Long> {


    /**
     * 普通的crud用接口提供的或者自定义方法名即可
     * 复杂的crud用 restTemplate 定义dsl进行查询
     * @param age
     * @return
     */


    List<Person> findAllByAgeGreaterThanEqual(Integer age);
    void deleteByAgeLessThan();
    long countByNameLike(String name);


}
