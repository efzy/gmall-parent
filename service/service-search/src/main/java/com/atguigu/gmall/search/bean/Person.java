package com.atguigu.gmall.search.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.checker.units.qual.A;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author lfy
 * @Description
 * @create 2022-12-12 10:36
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Document(indexName = "person")  //这是一个文档
public class Person {

    @Id  //主键
    private Long id;

    @Field(value = "name",type = FieldType.Text) //文本字段能全文检索
    private String name;

    @Field(value = "age",type = FieldType.Integer)
    private Integer age;
}
