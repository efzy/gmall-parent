package com.atguigu.gmall.starter.cache.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.starter.cache.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author lfy
 * @Description
 * @create 2022-12-06 14:30
 */

@Slf4j
@Service
public class CacheServiceImpl implements CacheService {
    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public Object getCacheData(String key, Type returnType) {
        //1、查询缓存
        String json = redisTemplate.opsForValue().get(key);
        //缓存没有
        if (StringUtils.isEmpty(json)) {
            return null;
        }else if("x".equals(json)){
            return new Object(); //返回空对象
        }else {
            log.info("缓存命中");
            return  JSON.parseObject(json,returnType);
        }
    }

    @Override
    public boolean mightContain(String bitMapName, Long bitMapIndex) {
        Boolean aBoolean = redisTemplate.opsForValue().getBit(bitMapName, bitMapIndex);
        return aBoolean.booleanValue();
    }

    @Override
    public void saveCacheData(String cacheKey, Object data, long ttl, TimeUnit unit) {
        String jsonString = "x";  //默认是x
        if(data != null){
            jsonString = JSON.toJSONString(data); //真正json
        }
        redisTemplate.opsForValue()
                .set(cacheKey,jsonString,ttl, unit);
    }

    ScheduledExecutorService pool = Executors.newScheduledThreadPool(16);
    @Override
    public void delayDoubleDel(String cacheKey) {
        redisTemplate.delete(cacheKey);
        //jvm：利用时间片算法进行调用
        pool.schedule(()->{
            redisTemplate.delete(cacheKey);
        },10,TimeUnit.SECONDS);
    }
}
