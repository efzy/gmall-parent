package com.atguigu.gmall.starter.cache.service;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

/**
 * @author lfy
 * @Description
 * @create 2022-12-06 14:30
 */
public interface CacheService {


    /**
     * 从缓存中获取指定类型的数据
     * @param s
     * @param returnType
     * @return
     */
    Object getCacheData(String s, Type returnType);

    /**
     * 判定指定位图bitMapName 中 有没有 bitMapIndex 位置的数据
     * @param bitMapName
     * @param bitMapIndex
     * @return
     */
    boolean mightContain(String bitMapName, Long bitMapIndex);

    /**
     * 给缓存中保存指定数据
     * @param cacheKey
     * @param data
     * @param ttl
     * @param unit
     */
    void saveCacheData(String cacheKey, Object data, long ttl, TimeUnit unit);


    /**
     * 延迟双删
     * @param cacheKey
     */
    void delayDoubleDel(String cacheKey);
}
