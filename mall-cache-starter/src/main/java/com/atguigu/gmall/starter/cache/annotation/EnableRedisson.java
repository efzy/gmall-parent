package com.atguigu.gmall.starter.cache.annotation;

import com.atguigu.gmall.starter.cache.redisson.RedissonAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author lfy
 * @Description
 * @create 2022-12-10 9:02
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import(RedissonAutoConfiguration.class)
public @interface EnableRedisson {
}
