package com.atguigu.gmall.user.vo;

import lombok.Data;

/**
 * @author lfy
 * @Description
 * @create 2022-12-15 10:43
 */
@Data
public class LoginSuccessVo {
    private String token;
    private Long userId;
    private String nickName;
}
