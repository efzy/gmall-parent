package com.atguigu.gmall.search.vo;

import lombok.Data;

/**
 * @author lfy
 * @Description 封装检索用的所有参数
 * @create 2022-12-12 14:00
 */
@Data
public class SearchParamVo {
    Long category1Id;
    Long category2Id;
    Long category3Id;
    //以上是分类相关参数  category3Id=61

    //关键字检索
    String keyword;  //keyword=华为

    //品牌检索
    String trademark; //trademark=2:华为


    //平台属性
    String[] props;
    //props=4:256GB:机身存储&props=3:8GB:运行内存

    //排序方式 1：综合（热度分）  2：价格
    //desc/asc
    String order = "1:desc"; //order=2:desc
    //

    //页码
    Integer pageNo = 1; //pageNo=1
}
