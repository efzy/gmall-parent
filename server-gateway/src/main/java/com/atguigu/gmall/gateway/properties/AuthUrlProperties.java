package com.atguigu.gmall.gateway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lfy
 * @Description
 * @create 2022-12-15 15:35
 */
@Component
@ConfigurationProperties(prefix = "app.auth")
@Data
public class AuthUrlProperties {

    //直接放行的请求
    private List<String> anyoneUrl;

    //任何情况都拒绝访问的
    private List<String> denyUrl;

    //必须认证通过（登录以后）才能访问的路径
    private List<String> authUrl;

    private String loginPage; //登录页地址
}
