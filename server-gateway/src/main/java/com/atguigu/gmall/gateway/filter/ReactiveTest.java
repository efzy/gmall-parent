package com.atguigu.gmall.gateway.filter;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lfy
 * @Description
 * @create 2022-12-15 14:19
 */
public class ReactiveTest {

    /**
     * 万物皆数据：
     * 1、0-1个：Mono
     * 2、N个： Flux
     * 数据皆为流
     * Mono： 发布0或1个数据的流发布者
     * Flux： 发布N个数据的流发布者
     * @param args
     */
    public static void aaaa(String[] args) throws InterruptedException {

        System.out.println("开始：主线程"+Thread.currentThread());

        Mono<String> mono = Mono.just("1");
        //1、数据发布者
//        Mono<String> aaa = Mono.fromCallable(()->{
//            System.out.println(Thread.currentThread()+"：正在准备一堆数据....");
//            Thread.sleep(3000);
//            System.out.println(Thread.currentThread()+"：数据准备完成....");
//           return "1111";
//        });
//
//        aaa.subscribe((item)->{
//            System.out.println(Thread.currentThread()+"：拿到数据："+item);
//        });


//        System.out.println("哈哈");

        //2、数据发布者。每秒1个
        Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));

        interval.subscribe(item->{
            System.out.println("a："+item);
        });
        interval.subscribe(item->{
            try {
                Thread.sleep(3000);
                System.out.println("b："+item);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });

        Thread.sleep(1000000);
    }

    private static List<String> prepareData() {
        return Arrays.asList("1","2");
    }

    static int sum(int i,int j){
        return i+j;
    }

    static void print(List a){
        System.out.println(a);
    }
}
